package com.app.abatest.domain;

import android.content.Context;

import io.reactivex.observers.DisposableObserver;
import timber.log.Timber;

/**
 * Created by Paco on 02/04/2018.
 */
public abstract class BaseConsumer<K> extends DisposableObserver<K> {

    private final Context context;

    public BaseConsumer(Context context) {
        this.context = context;
    }

    public BaseConsumer() {
        this.context = null;
    }

    @Override
    public void onNext(K k) {
    }

    @Override
    public void onError(Throwable e) {
        Timber.e("KO");
    }

    @Override
    public void onComplete() {

    }
}
