package com.app.abatest.domain.file;

import com.app.abatest.data.rest.RepositoryProxy;
import com.app.abatest.data.rest.RestConstants;
import com.app.abatest.domain.BaseUseCase;

import io.reactivex.Observable;

/**
 * Created by Paco on 02/04/2018.
 */

public class GetFile extends BaseUseCase<Boolean> {

    public GetFile(RepositoryProxy repositoryProxy) {
        super(repositoryProxy);
    }

    @Override
    public Observable<Boolean> build() {
        return repositoryProxy.getRepository().getFile(RestConstants.FILE_URL).map(responseBody
                -> true);
    }
}
