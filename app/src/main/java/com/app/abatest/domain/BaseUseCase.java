package com.app.abatest.domain;

import com.app.abatest.data.rest.RepositoryProxy;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.disposables.Disposables;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Paco on 02/04/2018.
 */

public abstract class BaseUseCase<K> {

    private Scheduler subscriberScheduler;
    private Scheduler observableScheduler;
    private Disposable disposable = Disposables.empty();
    protected RepositoryProxy repositoryProxy;


    public BaseUseCase() {
        this.subscriberScheduler = Schedulers.io();
        this.observableScheduler = AndroidSchedulers.mainThread();
    }

    public BaseUseCase(RepositoryProxy repositoryProxy) {
        this.repositoryProxy = repositoryProxy;
        this.subscriberScheduler = Schedulers.io();
        this.observableScheduler = AndroidSchedulers.mainThread();
    }

    public abstract Observable<K> build();

    public void subscribe(BaseConsumer<K> consumer) {
        this.disposable = this.build()
                .subscribeOn(subscriberScheduler)
                .observeOn(observableScheduler)
                .subscribeWith(consumer);
    }

    public void unsubscribe() {
        if (disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
        }
    }

    public boolean isUnsubscribed() {
        return disposable.isDisposed();
    }
}