package com.app.abatest.domain.row;

import com.app.abatest.data.local.model.RowLocal;
import com.app.abatest.data.rest.RepositoryProxy;
import com.app.abatest.domain.BaseUseCase;
import com.app.abatest.ui.view.model.Row;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

/**
 * Created by Paco on 02/04/2018.
 */

public class GetRows extends BaseUseCase<List<Row>> {

    public GetRows(RepositoryProxy repositoryProxy) {
        super(repositoryProxy);
    }

    @Override
    public Observable<List<Row>> build() {
        return repositoryProxy.getLocalRepository().getRows().map(rowLocals -> {
            List<Row> rows = new ArrayList<>();
            for (RowLocal rowLocal : rowLocals) {
                rows.add(new Row(rowLocal));
            }
            return rows;
        });
    }
}
