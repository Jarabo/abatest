package com.app.abatest.di.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by Paco on 02/04/2018.
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface PerView {

}