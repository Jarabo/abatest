package com.app.abatest.di.module;

import com.app.abatest.di.annotation.PerView;
import com.app.abatest.domain.file.GetFile;
import com.app.abatest.domain.row.GetRows;
import com.app.abatest.ui.mvi.state.DetailViewState;
import com.app.abatest.ui.mvi.state.MainViewState;
import com.app.abatest.ui.presenter.DetailPresenter;
import com.app.abatest.ui.presenter.MainPresenter;
import com.app.abatest.ui.view.navigation.Navigator;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Paco on 02/04/2018.
 */
@Module
public class PresenterModule {

    @Provides
    @PerView
    MainPresenter provideMainPresenter(Navigator navigator, MainViewState mainViewState, GetFile
            getFile, GetRows getRows) {
        return new MainPresenter(navigator, mainViewState, getFile, getRows);
    }

    @Provides
    @PerView
    DetailPresenter provideDetailPresenter(Navigator navigator, DetailViewState detailViewState) {
        return new DetailPresenter(navigator, detailViewState);
    }

}
