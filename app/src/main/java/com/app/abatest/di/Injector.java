package com.app.abatest.di;

import android.app.Activity;

import com.app.abatest.di.component.DaggerPresenterComponent;
import com.app.abatest.di.component.PresenterComponent;
import com.app.abatest.di.module.PresenterModule;
import com.app.abatest.di.module.UseCaseModule;
import com.app.abatest.ui.ABATestApplication;

/**
 * Created by Paco on 02/04/2018.
 */

public class Injector {

    public static PresenterComponent getPresenterComponent(Activity activity) {
        return DaggerPresenterComponent.builder()
                .applicationComponent(ABATestApplication.getInstance().getApplicationComponent())
                .presenterModule(new PresenterModule())
                .useCaseModule(new UseCaseModule(activity))
                .build();
    }
}
