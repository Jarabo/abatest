package com.app.abatest.di.module;

import android.content.Context;

import com.app.abatest.data.rest.RepositoryProxy;
import com.app.abatest.di.annotation.PerView;
import com.app.abatest.domain.file.GetFile;
import com.app.abatest.domain.row.GetRows;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Paco on 02/04/2018.
 */

@Module
public class UseCaseModule {

    private Context activity;

    public UseCaseModule(Context activity) {
        this.activity = activity;
    }

    @Provides
    @PerView
    GetFile provideGetFile(RepositoryProxy repositoryProxy) {
        return new GetFile(repositoryProxy);
    }

    @Provides
    @PerView
    GetRows provideGetRows(RepositoryProxy repositoryProxy) {
        return new GetRows(repositoryProxy);
    }
}
