package com.app.abatest.di.component;

import android.app.Application;

import com.app.abatest.data.repository.LocalRepository;
import com.app.abatest.data.repository.RestRepository;
import com.app.abatest.di.module.ApplicationModule;
import com.app.abatest.ui.view.navigation.Navigator;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Paco on 02/04/2018.
 */

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    Application provideApplication();

    void inject(Application application);

    RestRepository provideRestRepository();

    LocalRepository provideLocalRepository();

    Navigator provideNavigator();

}