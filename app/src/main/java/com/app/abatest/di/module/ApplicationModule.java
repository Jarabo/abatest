package com.app.abatest.di.module;

import android.app.Application;

import com.app.abatest.data.local.LocalProxy;
import com.app.abatest.data.repository.LocalRepository;
import com.app.abatest.data.repository.RestRepository;
import com.app.abatest.data.rest.RestAdapter;
import com.app.abatest.ui.view.navigation.Navigator;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Paco on 02/04/2018.
 */

@Module
public class ApplicationModule {

    private final Application application;

    public ApplicationModule(Application application) {
        this.application = application;
    }

    @Provides
    @Singleton
    public Application provideApplicationContext() {
        return this.application;
    }

    @Provides
    @Singleton
    public RestRepository provideRestRepository(RestAdapter restAdapter, LocalProxy localProxy) {
        return new RestRepository(restAdapter, localProxy);
    }

    @Provides
    @Singleton
    public LocalRepository provideLocalRepository(LocalProxy localProxy) {
        return new LocalRepository(localProxy);
    }

    @Provides
    @Singleton
    public Navigator provideNavigator() {
        return new Navigator(application.getBaseContext());
    }

    @Provides
    @Singleton
    public RestAdapter provideRestAdapter() {
        return new RestAdapter();
    }

    @Provides
    @Singleton
    public LocalProxy provideLocalProxy() {
        return new LocalProxy();
    }
}