package com.app.abatest.di.module;

import com.app.abatest.di.annotation.PerView;
import com.app.abatest.ui.mvi.state.DetailViewState;
import com.app.abatest.ui.mvi.state.MainViewState;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Paco on 02/04/2018.
 */

@Module
public class ViewStateModule {

    @Provides
    @PerView
    MainViewState provideHomeViewState() {
        return new MainViewState();
    }

    @Provides
    @PerView
    DetailViewState provideDetailViewState() {
        return new DetailViewState();
    }
}
