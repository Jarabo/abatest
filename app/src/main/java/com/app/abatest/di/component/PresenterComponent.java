package com.app.abatest.di.component;

import com.app.abatest.di.annotation.PerView;
import com.app.abatest.di.module.PresenterModule;
import com.app.abatest.di.module.UseCaseModule;
import com.app.abatest.di.module.ViewStateModule;
import com.app.abatest.ui.view.activity.DetailActivity;
import com.app.abatest.ui.view.activity.MainActivity;

import dagger.Component;

/**
 * Created by Paco on 02/04/2018.
 */

@PerView
@Component(modules = {PresenterModule.class, UseCaseModule.class, ViewStateModule.class},
        dependencies =
        {ApplicationComponent.class})
public interface PresenterComponent {

    void inject(MainActivity mainActivity);

    void inject(DetailActivity detailActivity);
}
