package com.app.abatest.data.local;

/**
 * Created by Paco on 03/04/2018.
 */

public class LocalProxyUtils {

    public static String removeQuoteMarks(String text) {
        return text.replace("\"", "");
    }

    public static String[] cleanLine(String finalLine) {
        return finalLine.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", -1);
    }

    public static String[] splitLines(String text) {
        return text.split("(?=(?:(?:[^\"]*\"){2})*[^\"]*$)\\n");
    }
}
