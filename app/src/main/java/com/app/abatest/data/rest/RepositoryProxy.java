package com.app.abatest.data.rest;

import android.app.Application;

import com.app.abatest.data.repository.ABATestRepository;
import com.app.abatest.data.repository.LocalRepository;
import com.app.abatest.data.repository.RestRepository;

import javax.inject.Inject;

/**
 * Created by Paco on 02/04/2018.
 */

public class RepositoryProxy {

    private Application context;
    private RestRepository restRepository;
    private LocalRepository localRepository;

    @Inject
    public RepositoryProxy(Application context, RestRepository restRepository, LocalRepository
            localRepository) {
        this.context = context;
        this.restRepository = restRepository;
        this.localRepository = localRepository;
    }

    public ABATestRepository getRepository() {
        return restRepository;
    }

    public ABATestRepository getLocalRepository() {
        return localRepository;
    }

}
