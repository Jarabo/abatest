package com.app.abatest.data.repository;

import com.app.abatest.data.local.model.RowLocal;

import java.util.List;

import io.reactivex.Observable;
import okhttp3.ResponseBody;

/**
 * Created by Paco on 02/04/2018.
 */

public interface ABATestRepository {

    Observable<ResponseBody> getFile(String url);

    Observable<List<RowLocal>> getRows();

}
