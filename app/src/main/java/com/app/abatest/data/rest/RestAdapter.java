package com.app.abatest.data.rest;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;

/**
 * Created by Paco on 02/04/2018.
 */

public class RestAdapter {

    private ABATestApiService breweryDBApiService;
    private ExecutorService mExecutorService;

    public RestAdapter() {
        this.breweryDBApiService = generateApiService();
    }

    private ABATestApiService generateApiService() {
        mExecutorService = Executors.newCachedThreadPool();
        Retrofit retrofit = new Retrofit.Builder()
/*
                .addCallAdapterFactory(RxErrorHandlingCallAdapterFactory.create())
*/.addCallAdapterFactory(RxJava2CallAdapterFactory.create())

                .baseUrl(RestConstants.BASE_URL)
/*
                .addConverterFactory(RestUtils.buildGSONConverter())
*/
                .callbackExecutor(mExecutorService)
                .client(generateClient())
                .build();
        return retrofit.create(ABATestApiService.class);
    }

    private OkHttpClient.Builder generateOkHttpClient() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.retryOnConnectionFailure(true);
        return httpClient;
    }

    private static OkHttpClient generateClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder();
        httpClientBuilder.addInterceptor(logging);
      /*  httpClientBuilder
                .addInterceptor(
                        chain -> {
                            Request request = chain.request();
                            HttpUrl url = request.url().newBuilder().addQueryParameter
                                    ( RestConstants.QUERY_PARAM_KEY, RestConstants
                                    .BREWERYDB_API_KEY).build();
                            request = request.newBuilder().url(url).build();
                            return chain.proceed(request);
                        });
*/
        return httpClientBuilder.build();
    }

    public ABATestApiService getService() {
        return breweryDBApiService;
    }
}