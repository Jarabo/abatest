package com.app.abatest.data.repository;

import com.app.abatest.data.local.LocalProxy;
import com.app.abatest.data.local.model.RowLocal;
import com.app.abatest.data.rest.RestAdapter;

import java.util.List;

import io.reactivex.Observable;
import okhttp3.ResponseBody;

/**
 * Created by Paco on 02/04/2018.
 */

public class RestRepository implements ABATestRepository {

    private RestAdapter restAdapter;
    private LocalProxy localProxy;

    public RestRepository(RestAdapter restAdapter, LocalProxy localProxy) {
        this.restAdapter = restAdapter;
        this.localProxy = localProxy;
    }

    @Override
    public Observable<ResponseBody> getFile(String url) {
        return restAdapter.getService().getFile(url).doOnNext(responseBody -> localProxy.store
                (responseBody));
    }

    @Override
    public Observable<List<RowLocal>> getRows() {
        //Not used, just if true rest
        return null;
    }
}
