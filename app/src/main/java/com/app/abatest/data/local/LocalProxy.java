package com.app.abatest.data.local;

import com.app.abatest.data.local.model.RowLocal;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import io.realm.Realm;
import okhttp3.ResponseBody;

/**
 * Created by Paco on 02/04/2018.
 */

public class LocalProxy {

    public void store(ResponseBody responseBody) {
        try {
            String content = responseBody.string();
            String[] rawData = LocalProxyUtils.splitLines(content);
            String[] lines = Arrays.copyOfRange(rawData, 1, rawData.length);
            for (String line : lines) {
                String[] cleanLine = LocalProxyUtils.cleanLine(line);
                cleanLine[0] = LocalProxyUtils.removeQuoteMarks(cleanLine[0]);
                if (cleanLine.length > 1) {
                    cleanLine[1] = LocalProxyUtils.removeQuoteMarks(cleanLine[1]);
                }
                Realm.getDefaultInstance().executeTransaction(realm -> {
                    RowLocal rowLocal = new RowLocal(cleanLine);
                    realm.copyToRealmOrUpdate(rowLocal);
                });
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<RowLocal> getRows() {
        return Realm.getDefaultInstance().where(RowLocal.class).findAll();
    }
}
