package com.app.abatest.data.local.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Paco on 02/04/2018.
 */

public class RowLocal extends RealmObject {

    @PrimaryKey
    private String name;

    private String description;

    private String imageUrl;

    public RowLocal(String[] rowInfo) {
        name = rowInfo[0];
        if (rowInfo.length > 1) {
            description = rowInfo[1];
        }
        if (rowInfo.length > 2) {
            imageUrl = rowInfo[2];
        }
    }

    public RowLocal() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

}
