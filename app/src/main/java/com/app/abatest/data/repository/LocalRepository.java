package com.app.abatest.data.repository;

import com.app.abatest.data.local.LocalProxy;
import com.app.abatest.data.local.model.RowLocal;

import java.util.List;

import io.reactivex.Observable;
import okhttp3.ResponseBody;

/**
 * Created by Paco on 02/04/2018.
 */

public class LocalRepository implements ABATestRepository {

    private LocalProxy localProxy;

    public LocalRepository(LocalProxy localProxy) {
        this.localProxy = localProxy;
    }

    @Override
    public Observable<ResponseBody> getFile(String url) {
        //Not used
        return null;
    }

    @Override
    public Observable<List<RowLocal>> getRows() {
        return Observable.fromCallable(() -> localProxy.getRows());
    }
}
