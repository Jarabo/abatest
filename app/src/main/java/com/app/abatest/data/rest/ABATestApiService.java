package com.app.abatest.data.rest;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.http.GET;
import retrofit2.http.Streaming;
import retrofit2.http.Url;

/**
 * Created by Paco on 02/04/2018.
 */

public interface ABATestApiService {

    @Streaming
    @GET
    Observable<ResponseBody> getFile(@Url String url);

}
