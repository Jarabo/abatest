package com.app.abatest.ui.view.custom;

import android.util.AttributeSet;

/**
 * Created by Paco on 03/04/2018.
 */

public interface BaseView {

    void initView(AttributeSet attrs);

}
