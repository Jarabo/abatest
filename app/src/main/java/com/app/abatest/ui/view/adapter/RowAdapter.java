package com.app.abatest.ui.view.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.app.abatest.ui.view.custom.RowItemView;
import com.app.abatest.ui.view.model.Row;

import java.util.List;

/**
 * Created by Paco on 03/04/2018.
 */

public class RowAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Row> rows;
    private Listener listener;

    public RowAdapter(List<Row> rows, Listener listener) {
        this.rows = rows;
        this.listener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        final RowItemView homeBeerItemView = new RowItemView(parent.getContext());
        RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams
                .MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        homeBeerItemView.setLayoutParams(lp);
        return new RowItemViewHolder(homeBeerItemView);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        ((RowItemViewHolder) holder).bind(rows.get(position));
    }

    @Override
    public int getItemCount() {
        return rows.size();
    }

    public void addItems(List<Row> beers) {
        this.rows.addAll(beers.subList(this.rows.size(), beers.size()));
        notifyDataSetChanged();
    }

    public void clear() {
        this.rows.clear();
        notifyDataSetChanged();
    }

    private class RowItemViewHolder extends RecyclerView.ViewHolder implements View
            .OnClickListener {

        public RowItemViewHolder(final View itemView) {
            super(itemView);
        }

        public void bind(final Row row) {
            final RowItemView rowItemView = ((RowItemView) itemView);
            rowItemView.setData(row);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            listener.onRowClicked(rows.get(getAdapterPosition()));
        }
    }

    public interface Listener {

        void onRowClicked(Row row);

    }
}