package com.app.abatest.ui.view.navigation;

import android.content.Context;
import android.content.Intent;

import com.app.abatest.ui.view.activity.DetailActivity;
import com.app.abatest.ui.view.model.Row;

/**
 * Created by Paco on 02/04/2018.
 */

public class Navigator {

    private Context context;

    public Navigator(Context context) {
        this.context = context;
    }

    public void launchRowDetail(Row row) {
        Intent intent = new Intent(context, DetailActivity.class);
        intent.putExtra(NavigationConstants.BUNDLE_KEY_ROW, row);
        context.startActivity(intent);

    }
}
