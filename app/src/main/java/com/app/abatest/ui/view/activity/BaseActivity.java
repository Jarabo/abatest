package com.app.abatest.ui.view.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import com.app.abatest.R;
import com.app.abatest.ui.mvi.contract.MVIContract;
import com.app.abatest.ui.presenter.BasePresenter;

import javax.inject.Inject;

/**
 * Created by Paco on 02/04/2018.
 */

public abstract class BaseActivity<P extends BasePresenter> extends AppCompatActivity implements
        MVIContract.MVIView {

    @Inject
    protected P presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectDependencies();
        presenter.register(this);
    }

    protected abstract void init();

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onResume();
    }

    @Override
    public void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        presenter.onPostCreate();
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.unregister();
    }

    protected void showError(Throwable error) {
        //Parse error as developer wishes
        showDialog(R.string.error_title, R.string.error_has_ocurred_text);
    }

    protected void showDialog(int title, int text) {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(getString(text));
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(android.R.string.ok),
                (dialog, which) -> dialog.dismiss());
        alertDialog.show();
    }

    protected abstract void injectDependencies();

    public abstract void showLoader(boolean show);
}