package com.app.abatest.ui.view.utils;

import android.widget.ImageView;

import com.app.abatest.R;
import com.squareup.picasso.Picasso;

/**
 * Created by Paco on 03/04/2018.
 */

public class ImageLoader {

    public static void loadImage(ImageView imageView, String url) {
        try {
            Picasso.get().load(url).placeholder(R.mipmap.ic_launcher).fit().into(imageView);
        } catch (IllegalArgumentException e) {
            imageView.setImageResource(R.mipmap.ic_launcher);
        }
    }
}
