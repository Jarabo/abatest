package com.app.abatest.ui.mvi.contract;

import com.app.abatest.ui.view.model.Row;

/**
 * Created by Paco on 03/04/2018.
 */

public interface DetailContract extends MVIContract {

    interface DetailView extends MVIView {
        void render(DetailState state);
    }

    interface DetailPresenter extends MVIPresenter<DetailView> {

        void setData(Row row);

    }

    interface DetailState extends State<Row> {

        @Override
        DetailState build();

        @Override
        DetailState setData(Row data);
    }
}
