package com.app.abatest.ui.presenter;

import com.app.abatest.ui.mvi.contract.DetailContract;
import com.app.abatest.ui.mvi.state.DetailViewState;
import com.app.abatest.ui.view.model.Row;
import com.app.abatest.ui.view.navigation.Navigator;

/**
 * Created by Paco on 03/04/2018.
 */

public class DetailPresenter extends BasePresenter<DetailContract.DetailView, DetailContract
        .DetailState> implements DetailContract.DetailPresenter {

    public DetailPresenter(Navigator navigator, DetailViewState state) {
        super(navigator, state);
    }

    @Override
    public void setData(Row row) {
        view.render(new DetailViewState(row));
    }
}
