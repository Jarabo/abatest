package com.app.abatest.ui.mvi.contract;

/**
 * Created by Paco on 02/04/2018.
 */

public interface MVIContract {

    interface MVIView {

    }

    interface MVIPresenter<T extends MVIView> {

        void register(T mvpView);

        void unregister();
    }

    interface State<P> {

        P getData();

        boolean isLoading();

        State<P> setData(P data);

        State<P> build();

        void clear();

        State<P> setError(Throwable error);

        Throwable getError();
    }
}