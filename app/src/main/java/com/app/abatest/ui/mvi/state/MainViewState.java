package com.app.abatest.ui.mvi.state;

import com.app.abatest.ui.mvi.contract.MainContract;
import com.app.abatest.ui.view.model.Row;

import java.util.List;

/**
 * Created by Paco on 03/04/2018.
 */

public class MainViewState extends BaseState<List<Row>> implements MainContract.MainState {

    private boolean hasTriedDownloadingFile;

    public MainViewState() {
    }

    @Override
    public MainViewState build() {
        isLoading = true;
        return this;
    }

    public MainViewState(List<Row> data) {
        super(data);
    }

    public MainViewState addData(List<Row> data) {
        if (this.data == null) {
            this.data = data;
        } else {
            this.data.addAll(data);
        }
        isLoading = false;
        return this;
    }


    @Override
    public Throwable getError() {
        return error;
    }

    public void setHasTriedDownloadingFile(boolean hasTriedDownloadingFile) {
        this.hasTriedDownloadingFile = hasTriedDownloadingFile;
    }

    @Override
    public boolean hasTriedDownloadingFile() {
        return hasTriedDownloadingFile;
    }
}
