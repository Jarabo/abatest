package com.app.abatest.ui.view.activity;

import android.os.Bundle;
import android.text.Html;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.abatest.R;
import com.app.abatest.di.Injector;
import com.app.abatest.ui.mvi.contract.DetailContract;
import com.app.abatest.ui.presenter.DetailPresenter;
import com.app.abatest.ui.view.navigation.NavigationConstants;
import com.app.abatest.ui.view.utils.ImageLoader;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailActivity extends BaseActivity<DetailPresenter> implements DetailContract
        .DetailView {

    @BindView(R.id.activity_detail_tv_name)
    protected TextView mTvName;
    @BindView(R.id.activity_detail_tv_description)
    protected TextView mTvDescription;
    @BindView(R.id.activity_detail_iv_thumb)
    protected ImageView mIvThumb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this);
        setSupportActionBar(findViewById(R.id.activity_detail_toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        init();
    }

    @Override
    protected void init() {
        presenter.setData(getIntent().getParcelableExtra(NavigationConstants.BUNDLE_KEY_ROW));

    }

    @Override
    protected void injectDependencies() {
        Injector.getPresenterComponent(this).inject(this);
    }

    @Override
    public void showLoader(boolean show) {

    }

    @Override
    public void render(DetailContract.DetailState state) {
        if (state.getData().getImageUrl() != null && !state.getData().getImageUrl().isEmpty()) {
            ImageLoader.loadImage(mIvThumb, state.getData().getImageUrl());
        }
        mTvName.setText(Html.fromHtml(state.getData().getName()));
        mTvDescription.setText(Html.fromHtml(state.getData().getDescription()));
    }
}
