package com.app.abatest.ui.view.activity;

import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.app.abatest.R;
import com.app.abatest.di.Injector;
import com.app.abatest.ui.mvi.contract.MainContract;
import com.app.abatest.ui.presenter.MainPresenter;
import com.app.abatest.ui.view.adapter.RowAdapter;
import com.app.abatest.ui.view.model.Row;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity<MainPresenter> implements MainContract.MainView,
        RowAdapter.Listener {

    @BindView(R.id.activity_main_rv_rows)
    protected RecyclerView mRvRows;
    @BindView(R.id.activity_main_pb_progress)
    protected View mPbProgress;

    private RowAdapter rowAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        init();
    }

    @Override
    protected void init() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext
                ());
        mRvRows.setLayoutManager(mLayoutManager);
        mRvRows.setItemAnimator(new DefaultItemAnimator());
    }

    @Override
    protected void injectDependencies() {
        Injector.getPresenterComponent(this).inject(this);
    }

    @Override
    public void showLoader(boolean show) {
        mPbProgress.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onRowClicked(Row row) {
        presenter.onRowClicked(row);
    }

    @Override
    public void render(MainContract.MainState state) {
        showLoader(state.isLoading());
        if (state.getData() != null) {
            if (rowAdapter == null) {
                rowAdapter = new RowAdapter(state.getData(), this);
                mRvRows.setAdapter(rowAdapter);
            }
        } else if (state.getData() == null && state.hasTriedDownloadingFile()) {
            showDialog(R.string.no_data_title, R.string.no_data_text);
        } else if (state.getError() != null) {
            showError(state.getError());
        }
    }

    @Override
    public void onFileDownloaded() {
        presenter.getRows();
    }
}
