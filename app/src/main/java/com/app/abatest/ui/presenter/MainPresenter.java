package com.app.abatest.ui.presenter;

import com.app.abatest.domain.BaseConsumer;
import com.app.abatest.domain.file.GetFile;
import com.app.abatest.domain.row.GetRows;
import com.app.abatest.ui.mvi.contract.MainContract;
import com.app.abatest.ui.mvi.state.MainViewState;
import com.app.abatest.ui.view.model.Row;
import com.app.abatest.ui.view.navigation.Navigator;

import java.util.List;

/**
 * Created by Paco on 02/04/2018.
 */

public class MainPresenter extends BasePresenter<MainContract.MainView, MainContract.MainState>
        implements MainContract.MainPresenter {

    private GetFile getFile;
    private GetRows getRows;
    private MainViewState mainViewState;

    public MainPresenter(Navigator navigator, MainViewState mainViewState, GetFile getFile,
                         GetRows getRows) {
        super(navigator);
        this.getFile = getFile;
        this.getRows = getRows;
        this.mainViewState = mainViewState;
    }

    @Override
    public void onPostCreate() {
        super.onPostCreate();
        mainViewState.setHasTriedDownloadingFile(false);
        view.render(mainViewState);
        getFile.subscribe(new BaseConsumer<Boolean>() {
            @Override
            public void onNext(Boolean aVoid) {
                super.onNext(aVoid);
                mainViewState.setHasTriedDownloadingFile(true);
                view.onFileDownloaded();
            }

            @Override
            public void onError(Throwable e) {
                super.onError(e);
                mainViewState.setHasTriedDownloadingFile(true);
                view.onFileDownloaded();
            }
        });
    }

    //Suitable for pagination against DB
    public void getRows() {
        getRows.subscribe(new BaseConsumer<List<Row>>() {
            @Override
            public void onError(Throwable e) {
                super.onError(e);
                mainViewState.setError(e);
                view.render(mainViewState);
            }

            @Override
            public void onNext(List<Row> rows) {
                super.onNext(rows);
                mainViewState.addData(rows);
                view.render(mainViewState);
            }
        });
    }

    public void onRowClicked(Row row) {
        navigator.launchRowDetail(row);
    }
}
