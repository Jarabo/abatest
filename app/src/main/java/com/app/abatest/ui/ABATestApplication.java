package com.app.abatest.ui;

import android.app.Application;

import com.app.abatest.di.component.ApplicationComponent;
import com.app.abatest.di.component.DaggerApplicationComponent;
import com.app.abatest.di.module.ApplicationModule;

import io.realm.Realm;
import timber.log.Timber;

/**
 * Created by Paco on 02/04/2018.
 */

public class ABATestApplication extends Application {

    private static ABATestApplication application;
    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        initUtils();
        initRealm();
        initGraph();
        application = this;
        applicationComponent.inject(this);
    }

    private void initRealm() {
        Realm.init(getBaseContext());
    }

    private void initUtils() {
        Timber.plant(new Timber.DebugTree());
    }

    private void initGraph() {
        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }

    public static ABATestApplication getInstance() {
        return application;
    }
}