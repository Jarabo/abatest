package com.app.abatest.ui.mvi.state;

import com.app.abatest.ui.mvi.contract.DetailContract;
import com.app.abatest.ui.view.model.Row;

/**
 * Created by Paco on 03/04/2018.
 */

public class DetailViewState extends BaseState<Row> implements DetailContract.DetailState {

    public DetailViewState() {
    }

    public DetailViewState(Row data) {
        super(data);
    }

    @Override
    public DetailViewState build() {
        return this;
    }

    @Override
    public Throwable getError() {
        return null;
    }

    @Override
    public DetailViewState setData(Row data) {
        super.setData(data);
        return this;
    }
}
