package com.app.abatest.ui.mvi.contract;

import com.app.abatest.ui.view.model.Row;

import java.util.List;

/**
 * Created by Paco on 02/04/2018.
 */

public interface MainContract extends MVIContract {

    interface MainView extends MVIView {

        void render(MainState mainState);

        void onFileDownloaded();
    }

    interface MainPresenter extends MVIPresenter<MainView> {

    }

    interface MainState extends State<List<Row>> {

        boolean hasTriedDownloadingFile();
    }
}
