package com.app.abatest.ui.mvi.state;

import com.app.abatest.ui.mvi.contract.MVIContract;

/**
 * Created by Paco on 02/04/2018.
 */

public abstract class BaseState<T> implements MVIContract.State<T> {

    protected T data;
    protected boolean isLoading = true;
    protected Throwable error;

    @Override
    public T getData() {
        return data;
    }

    @Override
    public boolean isLoading() {
        return isLoading;
    }

    @Override
    public BaseState<T> setData(T data) {
        this.data = data;
        this.isLoading = false;
        return this;
    }


    public BaseState(T data) {
        this.data = data;
        this.isLoading = false;
    }

    public BaseState() {

    }

    @Override
    public void clear() {
        this.data = null;
    }

    @Override
    public BaseState<T> setError(Throwable error) {
        this.error = error;
        this.isLoading = false;
        return this;
    }
}
