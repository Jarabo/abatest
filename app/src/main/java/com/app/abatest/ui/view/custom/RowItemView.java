package com.app.abatest.ui.view.custom;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.abatest.R;
import com.app.abatest.ui.view.model.Row;
import com.app.abatest.ui.view.utils.ImageLoader;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Paco on 03/04/2018.
 */

public class RowItemView extends CardView implements BaseView {

    @BindView(R.id.view_item_row_iv_image)
    protected ImageView mIvThumb;
    @BindView(R.id.view_item_row_tv_name)
    protected TextView mTvName;

    private Row row;

    public RowItemView(Context context) {
        this(context, null);
    }

    public RowItemView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RowItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(attrs);
    }

    @Override
    public void initView(AttributeSet attrs) {
        ButterKnife.bind(this, LayoutInflater.from(this.getContext()).inflate(R.layout
                .view_item_row, this, true));
        setUseCompatPadding(true);
        setRadius(10);
    }

    public void setData(Row row) {
        this.row = row;
        mTvName.setText(this.row.getName());
        if (this.row.getImageUrl() != null && !this.row.getImageUrl().isEmpty()) {
            ImageLoader.loadImage(mIvThumb, this.row.getImageUrl());
        }
    }
}