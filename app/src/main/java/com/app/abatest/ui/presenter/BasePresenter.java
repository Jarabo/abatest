package com.app.abatest.ui.presenter;

import com.app.abatest.ui.mvi.contract.MVIContract;
import com.app.abatest.ui.view.navigation.Navigator;

/**
 * Created by Paco on 02/04/2018.
 */
public abstract class BasePresenter<T extends MVIContract.MVIView, P extends MVIContract.State>
        implements MVIContract.MVIPresenter<T> {

    protected T view;
    protected P state;
    protected Navigator navigator;

    public BasePresenter(Navigator navigator, P state) {
        this.navigator = navigator;
        this.state = state;
    }

    public BasePresenter(Navigator navigator) {
        this.navigator = navigator;
    }

    @Override
    public void register(T mvpView) {
        this.view = mvpView;
    }

    @Override
    public void unregister() {
        this.view = null;
    }

    public void onResume() {
    }

    public void onStart() {
    }

    public void onPostCreate() {
    }

    public void clear() {
        if (state != null) {
            state.clear();
        }
    }
}
